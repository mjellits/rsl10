################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../source/app_bass.c \
../source/app_config.c \
../source/app_customss.c \
../source/app_msg_handler.c 

OBJS += \
./source/app_bass.o \
./source/app_config.o \
./source/app_customss.o \
./source/app_msg_handler.o 

C_DEPS += \
./source/app_bass.d \
./source/app_config.d \
./source/app_customss.d \
./source/app_msg_handler.d 


# Each subdirectory must supply rules for building sources it contributes
source/%.o: ../source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -Wall -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g -DRSL10_CID=101 -DSECURE_CONNECTION -DAPP_BONDLIST_SIZE=28 -DCFG_BOND_LIST_IN_NVR2=true -DCFG_BLE=1 -DCFG_ALLROLES=1 -DCFG_APP -DCFG_APP_BATT -DCFG_ATTS=1 -DCFG_CON=8 -DCFG_EMB=1 -DCFG_HOST=1 -DCFG_RF_ATLAS=1 -DCFG_ALLPRF=1 -DCFG_PRF=1 -DCFG_NB_PRF=2 -DCFG_CHNL_ASSESS=1 -DCFG_SEC_CON=1 -DCFG_EXT_DB -DCFG_PRF_BASS=1 -D_RTE_ -I"C:\Users\markusjellitsch\Documents\rsl10\BinauralLink\ble_peripheral_server_bond\include" -I"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/include" -I"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/include/bb" -I"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/include/ble" -I"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/include/ble/profiles" -I"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/include/kernel" -I"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/source/firmware/printf" -I"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/source/firmware/rtt" -I"C:\Users\markusjellitsch\Documents\rsl10\BinauralLink\ble_peripheral_server_bond/RTE" -I"C:\Users\markusjellitsch\Documents\rsl10\BinauralLink\ble_peripheral_server_bond/RTE/Device/RSL10" -I"C:\Users\markusjellitsch\Documents\rsl10\BinauralLink\ble_peripheral_server_bond/RTE/Utility" -isystem"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/include" -isystem"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/include/bb" -isystem"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/include/ble" -isystem"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/include/ble/profiles" -isystem"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/include/kernel" -isystem"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/source/firmware/printf" -isystem"C:/Users/markusjellitsch/AppData/Local/Arm/Packs/ONSemiconductor/ONSemiconductor/RSL10/3.2.608/source/firmware/rtt" -isystem"C:\Users\markusjellitsch\Documents\rsl10\BinauralLink\ble_peripheral_server_bond/RTE" -isystem"C:\Users\markusjellitsch\Documents\rsl10\BinauralLink\ble_peripheral_server_bond/RTE/Device/RSL10" -isystem"C:\Users\markusjellitsch\Documents\rsl10\BinauralLink\ble_peripheral_server_bond/RTE/Utility" -std=gnu11 -Wmissing-prototypes -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


